﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FinamTest.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FinamTest.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ReportController : Controller
    {
        private readonly ApplicationDbContext db;

        public ReportController(ApplicationDbContext db)
        {
            this.db = db;
        }
        
        [HttpGet]
        public async Task<IEnumerable<Group>> Get(int order = 1, 
            DateTime? startDate = null, 
            DateTime? endDate = null)
        {
            if (order < 1 || order > 3)
                BadRequest();
            
            var queryable = db.Incomes.Include(i => i.Category).AsNoTracking();
            if (startDate != null)
                queryable = queryable.Where(i => i.Date >= startDate);
            
            if (endDate != null)
                queryable = queryable.Where(i => i.Date <= endDate);

            var en = queryable.AsEnumerable();
            IEnumerable<IGrouping<string, Income>> grp = null;
            switch (order)
            {
                case 1:
                    grp = en.GroupBy(i => i.Category.Name);
                    break;
                case 2:
                    grp = en.GroupBy(i => GetMonth(i.Date.Month));
                    break;
                case 3:
                    grp = en.GroupBy(i => $"{i.Category.Name}: {GetMonth(i.Date.Month)}");
                    break;
            }

            return grp.Select(g => new Group {Name = g.Key, Incomes = g.ToList()}).ToList();
        }

        private string GetMonth(int m)
        {
            return m switch
            {
                1 => "Январь",
                2 => "Февраль",
                3 => "Март",
                4 => "Апрель",
                5 => "Май",
                6 => "Июнь",
                7 => "Июль",
                8 => "Август",
                9 => "Сентябрь",
                10 => "Октябрь",
                11 => "Ноябрь",
                12 => "Декабрь",
                _ => throw new ArgumentException()
            };
        }
    }
}