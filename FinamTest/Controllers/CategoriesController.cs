﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FinamTest.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FinamTest.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CategoriesController : Controller
    {
        private readonly ApplicationDbContext db;

        public CategoriesController(ApplicationDbContext db)
        {
            this.db = db;
        }
        
        [HttpGet]
        public async Task<IEnumerable<Category>> GetCategories()
        {
            return  await db.Categories.AsNoTracking().ToListAsync();
        }
        
        [HttpPost]
        public async Task<ActionResult<Category>> PostCategory(Category category)
        {
            if (string.IsNullOrEmpty(category.Name) || await db.Categories.AnyAsync(c => c.Name == category.Name))
                return BadRequest();

            
            await db.Categories.AddAsync(category);
            await db.SaveChangesAsync();
            return Ok(category);
        }

        [HttpPut]
        public async Task<ActionResult<Category>> PutCategory(Category category)
        {
            if (string.IsNullOrEmpty(category.Name) || await db.Categories.AnyAsync(c => c.Name == category.Name))
                return BadRequest();
            
            var obj = await db.Categories.AsNoTracking().FirstOrDefaultAsync(c => c.Id == category.Id);
            if (obj == null)
                return BadRequest();
            
            db.Categories.Update(category);
            await db.SaveChangesAsync();
            return Ok(category);
        }
        
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteCategory(int id)
        {
            var obj = await db.Categories.FirstOrDefaultAsync(c => c.Id == id);
            if (obj == null)
                return BadRequest();

            db.Categories.Remove(obj);
            await db.SaveChangesAsync();
            return Ok();
        }
    }
}