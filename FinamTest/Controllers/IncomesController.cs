﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FinamTest.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FinamTest.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class IncomesController : Controller
    {
        private readonly ApplicationDbContext db;

        public IncomesController(ApplicationDbContext db)
        {
            this.db = db;
        }
        
        [HttpGet]
        public async Task<IEnumerable<Income>> Get()
        {
            return await db.Incomes.Include(i => i.Category).AsNoTracking().ToListAsync();
        }
        
        [HttpPost]
        public async Task<ActionResult<Income>> Post(Income income)
        {
            var cat = await db.Categories.FirstOrDefaultAsync(c => c.Id == income.CategoryId);
            if (cat == null)
                return BadRequest();

            income.Category = cat;
            income.Date = DateTime.Now;
            await db.Incomes.AddAsync(income);
            await db.SaveChangesAsync();
            return Ok(income);
        }

        [HttpPut]
        public async Task<ActionResult<Income>> Put(Income income)
        {
            var obj = await db.Incomes.AsNoTracking().FirstOrDefaultAsync(i => i.Id == income.Id);
            if (obj == null)
                return BadRequest();
            
            var cat = await db.Categories.FirstOrDefaultAsync(c => c.Id == income.CategoryId);
            if (cat == null)
                return BadRequest();

            income.Category = cat;
            income.Date = obj.Date;
            db.Incomes.Update(income);
            await db.SaveChangesAsync();
            return Ok(income);
        }
        
        [HttpDelete]
        [Route("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var obj = await db.Incomes.FirstOrDefaultAsync(i => i.Id == id);
            if (obj == null)
                return BadRequest();

            db.Incomes.Remove(obj);
            await db.SaveChangesAsync();
            return Ok();
        }
    }
}