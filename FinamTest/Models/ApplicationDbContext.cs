﻿using Microsoft.EntityFrameworkCore;

namespace FinamTest.Models
{
    public sealed class ApplicationDbContext : DbContext
    {
        public DbSet<Income> Incomes { get; set; }
        public DbSet<Category> Categories { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
            Database.EnsureCreated();
        }
    }
}