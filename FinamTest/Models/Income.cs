﻿using System;

namespace FinamTest.Models
{
    public class Income
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public int Sum { get; set; }
        public string Comment { get; set; }

        public int  CategoryId { get; set; }
        public Category Category { get; set; }
    }
}