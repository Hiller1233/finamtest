﻿using System.Collections.Generic;

namespace FinamTest.Models
{
    public class Group
    {
        public string Name { get; set; }
        public List<Income> Incomes { get; set; }
    }
}